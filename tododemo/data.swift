//
//  data.swift
//  tododemo
//
//  Created by SGI-Mac7 on 11/10/2018.
//  Copyright © 2018 Slash Global. All rights reserved.
//

import Foundation
import UIKit
class data{
    private var label :String = ""
    private var subLabel :String = ""
    private var image:UIImage?
    
    
    var getlabel:String{
        return label
    }
    var getSublabel:String{
        return subLabel
    }
    var getImage:UIImage{
        return image!
    }
    
    init(label:String,sublabel:String,image:UIImage){
        self.label = label
        self.subLabel = sublabel
        self.image = image
        
    }
}
