//
//  Add.swift
//  tododemo
//
//  Created by SGI-Mac7 on 11/10/2018.
//  Copyright © 2018 Slash Global. All rights reserved.
//

import UIKit
protocol senddatabackdelegate:class{
    func sendback(with _data : data)
    
    
}

class Add: UIViewController {
    let imagePicker = UIImagePickerController()
    weak var delegate : senddatabackdelegate?
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var label: UITextField!
    @IBOutlet weak var subLabel: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        imagePicker.delegate = self
    }

    @IBAction func add(_ sender: Any) {
        
        navigationController?.popViewController(animated: true)
        let d = data(label: label.text!, sublabel: subLabel.text!, image: imageView.image!)
        print(d.getlabel)
        delegate?.sendback(with: d)
        
    }
    

    @IBAction func uploadBttnTapped(_ sender: Any) {
        imagePicker.allowsEditing = false
        imagePicker.sourceType = .photoLibrary
        present(imagePicker, animated: true, completion: nil)
        
    }
    
    
}
extension Add : UIImagePickerControllerDelegate,UINavigationControllerDelegate{
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
// Local variable inserted by Swift 4.2 migrator.
let info = convertFromUIImagePickerControllerInfoKeyDictionary(info)

        if let pickedImage = info[convertFromUIImagePickerControllerInfoKey(UIImagePickerController.InfoKey.originalImage)] as? UIImage {
            imageView.contentMode = .scaleAspectFill
            imageView.image = pickedImage
        }
        dismiss(animated: true, completion: nil)

    }
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion:nil)
    }
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKeyDictionary(_ input: [UIImagePickerController.InfoKey: Any]) -> [String: Any] {
	return Dictionary(uniqueKeysWithValues: input.map {key, value in (key.rawValue, value)})
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKey(_ input: UIImagePickerController.InfoKey) -> String {
	return input.rawValue
}
