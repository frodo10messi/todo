//
//  ViewController.swift
//  tododemo
//
//  Created by SGI-Mac7 on 11/10/2018.
//  Copyright © 2018 Slash Global. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    var dataArray = [data]()
    @IBOutlet weak var tableview: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableview.delegate = self
        tableview.dataSource = self
        tableview.rowHeight = UITableView.automaticDimension
        tableview.estimatedRowHeight = 140
        
    }
    override func viewWillAppear(_ animated: Bool) {
        tableview.reloadData()
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let destinationvc = segue.destination as! Add
        destinationvc.delegate = self
    }

}
extension ViewController:UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableview.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! TableViewCell
        
       let dataclass = dataArray[indexPath.row]
        
        cell.title.text = dataclass.getlabel
        cell.img.image = dataclass.getImage
        cell.subtitle.text = dataclass.getSublabel
        
        return cell
    }
    
    
}
extension ViewController : senddatabackdelegate{
    func sendback(with _data: data) {
        dataArray.append(_data)
    }
}

